# Software Studio 2020 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Midterm-Project-106030001
* Key functions (add/delete)
    1. Login
    2. Sign in with Google
    3. Signup
    4. Logout
    5. Notification
    6. Add Post
    7. Post
    8. Comment
    9. Input html message
    10. RWD
    
* Other functions (add/delete)
    1. Index
    2. Sign in with Facebook
    3. Profile
    4. Age count
    5. send friend request

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|N|
|Firebase Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|20%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|

## Website Detail Description

# 作品網址：https://midterm-project-106030001-1.web.app

# Components Description : 
Home page:

![image](https://gitlab.com/106030001/midterm-project-106030001/-/raw/master/img/img01.png)

1. Login : 用戶登入，可以使用email&password、google、facebook三種方式登入，使用email&password方法註冊的人輸入email和passwors即可登入。
2. Sign in with Google : 使用google登入，第一次登入時(用ref.orderByChild(‘Uid’).equalTo來check user.uid在database中是否已存在Profile，有的話直接回到首頁，沒有則跳至setup.html)，輸入Nickname、Birthday、Gender以建立Profile，Nickname也會使用和check uid一樣的方法避免有重複的nickname。
3. Signup : 使用email及password建立account，並且輸入Nickname、Birthday、Gender來建立Profile。成功後會自動跳回首頁。
4. Logout : 登入成功後，在首頁及觀看聊天室的界面都會有Logout的button。
5. Notification : 在收到交友邀請或是對方同意交友邀請之後，首頁會出現Notifications，在按下Accept之後會跳出視窗，如果notification失敗，會跳出alert請你check notification permission。

![image](https://gitlab.com/106030001/midterm-project-106030001/-/raw/master/img/img3.png)

6. Add Post : 新增一個chat room，輸入title及內容，並會儲存創建的時間及創建人的nickname。submit後會自動跳至剛剛創建的chat room，chat room用post的方式傳送資料，location.href="post.html?=" + post的id;。

![image](https://gitlab.com/106030001/midterm-project-106030001/-/raw/master/img/img5.png)

7. Post : 顯示chat room。先取得location.href之後解析出'='後面的post id，再查找database中符合該id的post和comment，並用forEach顯示所有comment。
8. Comment : 在每一個chat room最下面有輸入框可以留言並立即顯示，同樣會儲存時間及留言者的nickname。

![image](https://gitlab.com/106030001/midterm-project-106030001/-/raw/master/img/img4.png)

9. Input html message : 在Add post和comment中如果偵測到同時輸入'<'和'>'，就把'<'和'>'都刪除(replace成'')再儲存到database並顯示。(上圖為修改前後，下面輸入的是<h>tt</h>)
10. RWD : 主要用boostrap的class實作

# Other Functions Description : 
1. Index : 最上方會用粗體顯示登入帳號的nickname。下方列出每個chat room的Title、chat roon的創建者、創建時間、留言數、最後留言者、最後留言者的留言時間。
2. Sign in with Facebook : 使用facebook登入，功能及實作都與Sign in with Google大致相同，只是需要到facebook developer申請帳號取得Oauth。

![image](https://gitlab.com/106030001/midterm-project-106030001/-/raw/master/img/img2.png)

3. Profile : 可以查看個人檔案。首頁使用location.href="profile.html?=" + user的uid;轉入，解析網址之後取得'='後面的uid，在Profile中使用orderByKey().equalTo(user.uid).on(....(以下略)來找出該user的profile並顯示出來。可以傳送交友邀請。最下方顯示的是好友數量及好友列表。
4. Age count : 使用今天日期與Profile中的Birthday相減自動計算出目前年齡。
5. send friend request : 寄送交友邀請。輸入對方nickname即可寄送交友邀請(這也是為什麼nickname不能重覆，否則會找到多筆資料)，對方的notification會新增"XXX send you a friend request"的訊息，對方同意後自己這邊會顯示"XXX accept your friend request"。每個notification都會有一個key記錄訊息的類型，被寄送邀請的話最後面會多出Accept和Refuse的button，對方接受邀請的話則單純顯示訊息不加button。

## Security Report (Optional)
