$(function () {
  'use strict';

  $('[data-toggle="offcanvas"]').on('mousemove', function () {
    $('.offcanvas-collapse').toggleClass('open');
  });
});