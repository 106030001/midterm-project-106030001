function initApp() {
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnFacebook = document.getElementById('btnfacebook');

    btnLogin.addEventListener('click', function() {
        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value)
        .then(function(user) {
            window.location = "/";
        }).catch(function(error) {
            var errorMessage = error.message;
            console.log(errorMessage);
            txtEmail.value = '';
            txtPassword.value = '';
        });
    });

    btnGoogle.addEventListener('click', function() {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) {
            var user = result.user;
            var userRef = firebase.database().ref('Profile');
            userRef.orderByChild("Uid").equalTo(user.uid).once("value", snapshot => {
                if(snapshot.exists()) {
                    location.href = "index.html";
                } else {
                    location.href = "setup.html";
                }
            });
        }).catch(function(error) {
            var errorMessage = error.message;
            console.log(errorMessage);
        });
    });

    btnFacebook.addEventListener('click', function() {
        var provider = new firebase.auth.FacebookAuthProvider();
        provider.setCustomParameters({
          'display': 'popup'
        });
        firebase.auth().signInWithPopup(provider)
        .then(function(result) {
            var user = result.user;
            var userRef = firebase.database().ref('Profile');
            userRef.orderByChild("Uid").equalTo(user.uid).once("value", snapshot => {
                if(snapshot.exists()) {
                    location.href = "index.html";
                } else {
                    location.href = "setup.html";
                }
            });
        }).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(errorMessage);

});
    });

}

window.onload = function() {
    initApp();
};