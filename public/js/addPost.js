function initApp() {

    firebase.auth().onAuthStateChanged(function(user) {
        if(user) {
            var username = document.getElementById("username");
            var btnLogout = document.getElementById("logout-btn");
            var btnSubmit = document.getElementById("btnSubmit");
            var displayName;
            firebase.database().ref('Profile').child(user.uid).once('value', snapshot => {
                displayName = snapshot.val().Nickname;
            }).then(function() {
                console.log(displayName);
                username.innerHTML = displayName;
                btnLogout.addEventListener('click', function() {
                    firebase.auth().signOut()
                    .then(function(success) {
                        alert('Logout Success');
                        location.href = "index.html";
                    }).catch(function(error) {
                        alert('logout Error');
                    });
                });

                
                btnSubmit.addEventListener('click', function() {
                    var title = document.getElementById("post-title").value;
                    var content = document.getElementById("post-content").value;
                    var comment = {};
                    var new_id = 1;
                    if(title != null && content != null) {
                        firebase.database().ref('Count').once("value", snapshot => {
                            console.log(snapshot.val()+'aaa');
                            if(snapshot.val().posts != null) {
                                var current_id = snapshot.val().posts;
                                new_id = current_id + 1;
                                var n = {
                                    posts: new_id
                                }
                                firebase.database().ref('Count').set(n);
                                console.log(snapshot.val().posts+'bbb');
                                var time = getCurrentDate();
                                console.log(time);
                                var post = {
                                    title: title,
                                    author: displayName,
                                    time: time,
                                    content: content,
                                    comment_num: 0,
                                    last_comment: displayName,
                                    update: time
                                }
                                
                                firebase.database().ref('Post').child(snapshot.val().posts).set(post);
                                location.href = "post.html?=" + snapshot.val().posts;
                            } else {                            
                                new_id = 1;
                                var n = {
                                    posts: new_id
                                }
                                console.log(n);
                                firebase.database().ref('Count').set(n);
                                console.log(snapshot.val().posts+'ccc');
                                var time = getCurrentDate();
                                console.log(time);
                                var post = {
                                    title: title,
                                    author: displayName,
                                    time: time,
                                    content: content,
                                    comment_num: 0,
                                    last_comment: displayName,
                                    update: time
                                }
                                
                                firebase.database().ref('Post').child(snapshot.val().posts).set(post);
                                location.href = "post.html?=" + snapshot.val().posts;

                            }
                        }).catch(error => {
                            alert(error.message);
                        });
                        
                    } else {
                        alert("Please input something!");
                    }
                    
                });
            });
            
        }
    });

}

function getCurrentDate() {
    var currentdate = new Date();
    return currentdate.getFullYear() + "/" + currentdate.getMonth() + "/" + currentdate.getDay() + " " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function() {
    initApp();
};