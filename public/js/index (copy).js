function init() {
    var user_email = '';
    var addPost = document.getElementById('addPost');
    var post_list = document.getElementById('post_list');
    var p = "";

    firebase.auth().onAuthStateChanged(function(user) {
        var navbar = document.getElementById('nav-bar');
        // Check user login
        if (user) {
            user_email = user.email;
            navbar.innerHTML = "<span>" + user.displayName + "</span><button id='profile-btn'>Profile</button> <button id='logout-btn'>Logout</button>";
            var btnLogout = document.getElementById("logout-btn");
            var btnProfile = document.getElementById("profile-btn");
            
            btnLogout.addEventListener('click', function() {
                firebase.auth().signOut()
                .then(function(success) {
                    alert('Logout Success');
                }).catch(function(error) {
                    alert('logout Error');
                });
            });
            btnProfile.addEventListener('click', function() {
                let profileURL = new URL('index.html');
                var sid = new URLSearchParams({id: user.uid});
                profile.search = sid;
                location.href = profile.href;
            });
            
        } else {
            // It won't show any post if not login
            //menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            //document.getElementById('post_list').innerHTML = "";
        }
    });

    addPost.addEventListener('click', function() {
        firebase.auth().onAuthStateChanged(function(user) {
            if(user) {
                location.href = "addPost.html";
            } else {
                alert("Please Login/Signin First!");
            }
        });  
    });
    
    firebase.database().ref('Post').on("child_added", snapshot => {
        snapshot.forEach(item => {
            p += '<tr><td>' + item.val().title + '</td><td>' + item.val().author + '</td><td>' + item.val().time + '</td><td>' + item.val().comment_num + '</td><td>' + item.val().last_post + '</td><td>' + item.val().update + '</td></tr>';
        });
        post_list.innerHTML = p;
    });




}

window.onload = function() {
    init();
};