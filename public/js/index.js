function init() {
    var user_email = '';
    var addPost = document.getElementById('addPost');
    var post_list = document.getElementById('post_list');
    var p = "";

    firebase.auth().onAuthStateChanged(function(user) {
        var navbar = document.getElementById('nav-bar');
        if (user) {
            user_email = user.email;
            console.log(user.uid);
            var displayName;
            var username;
            firebase.database().ref('Profile').child(user.uid).once('value', snapshot => {
                displayName = snapshot.val().Nickname;
            }).then(function() {
                navbar.innerHTML = "<strong><span>" + displayName + "</span></strong> <button id='profile-btn' onclick='location.href=" + '"profile.html?=" + ' + user.uid + ';' + "'>Profile</button> <button id='logout-btn'>Logout</button> <button class='navbar-toggler p-0 border-0' type='button' data-toggle='offcanvas'><span class='navbar-toggler-icon'></span></button>";
                $('#nav-bar').append('<div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault"><ul class="navbar-nav mr-auto"><li class="nav-item dropdown"><a class="nav-link dropdown-toggle" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Notification</a><div id="dynamic-menu" class="dropdown-menu" aria-labelledby="dropdown01"></div></li></ul></div>');
                username = displayName;
                var btnProfile = document.getElementById("profile-btn");
                btnProfile.addEventListener('click', function() {
                    location.href = "profile.html?=" + user.uid;
                });
                firebase.database().ref('Notification').child(displayName).once('value', snapshot => {
                    $('#dynamic-menu').append('<span class="dropdown-item">456</span>');
                    console.log(snapshot.key);
                    if(snapshot.exists()) {
                        $('#notify').append("<br>Notification:<br>");
                        snapshot.forEach(item => {
                            console.log(item.key);
                            if(item.val().genre == 1) {
                                $('#notify').append("<span>" + item.val().txt + "</span> / <button id='accept' onclick='notification(" + item.val().name + ", " + item.val().id + ", " + displayName + ", " + user.uid + ", " + item.val().key + ");'>Accept</button>  <button id='refuse' onclick='refuse(" + item.val().name + ", " + displayName + ", " + item.val().key + ");'>Refuse</button><br><br>");
                                var accept = document.getElementById('accept');
                                var refuse_c = document.getElementById('refuse');
                                accept.addEventListener('click', function() {
                                    notification(item.val().name, item.val().id, displayName, user.uid, item.val().key);
                                });
                                refuse_c.addEventListener('click', function() {
                                    refuse(item.val().name, displayName, item.val().key);
                                });
                            }
                            else if(item.val().genre == 2) {
                                $('#notify').append("<span>" + item.val().txt + "</span>");
                            }
                            
                            $('#dynamic-menu').append("<span class='dropdown-item'>" + item.val().txt + "</span>");
                        });
                    } else {
                        $('#dynamic-menu').append('<a class="dropdown-item" href="#">None</a>');
                    }


                }).catch(e => {
                    console.log(e.message);
                });
                var btnLogout = document.getElementById("logout-btn");
                
                var noti = document.getElementById("dynamic-menu");
                $('#dynamic-menu').append('<span class="dropdown-item">123</span>');
                
                btnLogout.addEventListener('click', function() {
                    firebase.auth().signOut()
                    .then(function(success) {
                        alert('Logout Success');
                    }).catch(function(error) {
                        alert('logout Error');
                    });
                });
            });
            

        } else {
            navbar.innerHTML = "<a id='login-btn' href='login.html'>Login</a><a id='signup-btn' href='signup.html'>Signup</a>"
            // It won't show any post if not login
            //menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            //document.getElementById('post_list').innerHTML = "";
        }
    });

    addPost.addEventListener('click', function() {
        firebase.auth().onAuthStateChanged(function(user) {
            if(user) {
                location.href = "addPost.html";
            } else {
                alert("Please Login/Signin First!");
            }
        });  
    });
    firebase.database().ref().orderByKey().equalTo('Post').once("child_added", snapshot => {
        snapshot.forEach(item => {
            console.log(item.val());
            $('#post-list').append('<a href="#" id="btnPost" onclick="location.href=' + "'post.html?=' + " + item.key + ';">' + item.val().title + '</a>');
            $('#post-list').append(' / ' + item.val().author + ' / ' + item.val().time + ' / ' + item.val().comment_num + ' / ' + item.val().last_post + ' / ' + item.val().update + '<br>');
        });
        /*var btnPost = document.getElementById('btnPost');
        btnPost.addEventListener('click', function() {
            firebase.auth().onAuthStateChanged(function(user) {
                if (!user) {
                    alert("Please login/signup first!");
                    location.href = this.href;
                }
            });
        });*/
    }).catch(error => {
        console.log(error.message);
    });
}

function notification(acceped, aid, user, uid, nkey) {
    if (Notification && Notification.permission !== "granted") {
        Notification.requestPermission(function (status) {
            if (Notification.permission !== status) {
                Notification.permission = status;
            }
        });
    }
    if (Notification && Notification.permission === "granted") {
        var n = new Notification("You accept " + accepted + "'s friend request!");
        firebase.database().ref('Profile').orderByKey().equalTo(aid).once(snapshot => {
            snapshot.val().Friends++;
        });
        firebase.database().ref('Profile').orderByKey().equalTo(uid).once(snapshot => {
            snapshot.val().Friends++;
        });
        firebase.database().ref('Friend').child(accepted).update({user: "OK"});
        firebase.database().ref('Friend').child(user).update({accepted: "OK"});
        firebase.database().ref('Notification').child(user).child(nkey).remove();
    } else if (Notification && Notification.permission !== "denied") {
        Notification.requestPermission(function (status) {
            if (Notification.permission !== status) {
                Notification.permission = status;
            }
        // If the user said okay
            if (status === "granted") {
                var n = new Notification("You accept " + accepted + "'s friend request!");
                firebase.database().ref('Profile').orderByKey().equalTo(aid).once(snapshot => {
                    snapshot.val().Friends++;
                });
                firebase.database().ref('Profile').orderByKey().equalTo(uid).once(snapshot => {
                    snapshot.val().Friends++;
                });
                firebase.database().ref('Friend').child(accepted).child(user).update({status: "OK"});
                firebase.database().ref('Friend').child(user).child(accepted).update({status: "OK"});
                firebase.database().ref('Notification').child(user).child(nkey).remove();
            } else {
        // Otherwise, we can fallback to a regular modal alert
                alert("Please re-login and allow the Notification permission!");
                firebase.auth().signOut()
                .then(function(success) {
                    alert('Logout Success');
                }).catch(function(error) {
                    alert('logout Error');
                });
            }
        });
    } else {
      // We can fallback to a regular modal alert
        alert("Please re-login and allow the Notification permission!");
        firebase.auth().signOut()
        .then(function(success) {
            alert('Logout Success');
        }).catch(function(error) {
            alert('logout Error');
        });
    }
}

function refuse(refused, user, nkey) {
    var removed1 = firebase.database().ref('Friend').child(user).orderByKey().equalTo(refused);
    removed1.remove();
    var removed2 = firebase.database().ref('Friend').child(refused).orderByKey().equalTo(user);
    removed2.remove();
    firebase.database().ref('Notification').child(user).child(nkey).remove();
}

window.onload = function() {
    init();
};