function initApp() {
    console.log("signup");
    var txtNickname = document.getElementById('inputNickname');
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var dateBirthday = document.getElementById('inputBirthday');
    var genderFemale = document.getElementById('female');
    var genderMale = document.getElementById('male');
    var genderCustom = document.getElementById('custom');
    var inputGender = document.getElementById('inputGender');
    var btnSignUp = document.getElementById('btnSignUp');
/*

    genderFemale.addEventListener('change', function() {
        if(!this.checked) {
            inputGender.innerHTML = "";
        }
    });
    genderMale.addEventListener('change', function() {
        if(!this.checked) {
            inputGender.innerHTML = "";
        }
    });
    genderCustom.addEventListener('change', function() {
        if(this.checked) {
        }
    });
    inputGender.addEventListener('input', function(e) {
        genderCustom.checked;
    });*/

    btnSignUp.addEventListener('click', function() {
        var nickname = txtNickname.value;
        var email = txtEmail.value;
        var password = txtPassword.value;
        var birthday = dateBirthday.value;
        var profileRef = firebase.database().ref('Profile');
        var pfriendsRef = firebase.database().ref('Friends');
        var friends = {};
        var notification = {};
        console.log(birthday);
        if(nickname!=null && birthday!=null) {
            if(genderFemale.checked || genderMale.checked || genderCustom.checked) {
                var b = new Date(birthday.replace(/-/g, "\/")); 
                var d = new Date(); 
                var age = d.getFullYear()-b.getFullYear()-((d.getMonth()<b.getMonth()|| d.getMonth()==b.getMonth() && d.getDate()<b.getDate())?1:0);
                console.log(age);
                firebase.auth().createUserWithEmailAndPassword(email, password)
                .then(function(result) {
                    console.log(result.user.uid);
                    var user = result.user;
                    //alert(user.uid + ' + ' + user.displayName);
                    if(genderFemale.checked) {
                        var profile = {
                            Uid: user.uid,
                            Nickname: nickname,
                            Gender: "Female",
                            Age: age,
                            Birthday: birthday,
                            Friends: 0
                        };
                    } else if(genderMale.checked) {
                        var profile = {
                            Uid: user.uid,
                            Nickname: nickname,
                            Gender: "Male",
                            Age: age,
                            Birthday: birthday,
                            Friends: 0
                        };
                    } else {
                        var profile = {
                            Uid: user.uid,
                            Nickname: nickname,
                            Gender: "Other",
                            Age: age,
                            Birthday: birthday,
                            Friends: 0
                        };
                    }
                    console.log(user.uid);
                    firebase.database().ref('Profile').child(user.uid).set(profile);
                    firebase.database().ref('Friend').child(nickname).set(friends);
                    firebase.database().ref('Notification').child(nickname).set(notification);
                    console.log('success');
                    alert(user.displayName);
                    location.href = "index.html";
                    return firebase.auth().currentUser.updateUser({
                        displayName: nickname
                    });
                }).catch(function(error) {
                    var errorMessage = error.message;
                    console.log(errorMessage);
                    //txtEmail.value = '';
                    //txtPassword.value = '';
                });
            }
        }
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function() {
    initApp();
};