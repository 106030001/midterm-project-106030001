function initApp() {
    var uid = document.getElementById('uid');
    var nickname = document.getElementById('nickname');
    var gender = document.getElementById('gender');
    var age = document.getElementById('age')
    var brthday = document.getElementById('birthday');
    var friend_num = document.getElementById('friend-num');
    var friend_list = document.getElementById('friend-list');
    var requesting_list = document.getElementById('requesting-list');
    var addFriend = document.getElementById('addFriend');
    var result = document.getElementById('result');
    var btnProfile = document.getElementById('profile-btn');
    var user = firebase.auth().currentUser;

    firebase.auth().onAuthStateChanged(function(user) {
        if(user) {
            var username_display = document.getElementById("username");
            var btnLogout = document.getElementById("logout-btn");
            var displayName;
            firebase.database().ref('Profile').child(user.uid).once('value', snapshot => {
            	displayName = snapshot.val().Nickname;
            }).then(function() {
            	username_display.innerhtml = displayName;
	            btnLogout.addEventListener('click', function() {
	                firebase.auth().signOut()
	                .then(function(success) {
	                    alert('Logout Success');
	                    location.href = "index.html";
	                }).catch(function(error) {
	                    alert('logout Error');
	                });
	            });
	            btnProfile.addEventListener('click', function() {
	                location.href = "profile.html?=" + user.uid;
	            });
	            console.log(user.uid);
	            var sUrl = location.href;
			    console.log(sUrl);

			    if(sUrl.indexOf("?") != -1) {
			    	var gets = sUrl.split("?");
			    	console.log(gets);
			    	var getid = gets[1].split("=");
			    	var id = getid[1];
			    	console.log(id);
			    }

			    firebase.database().ref('Profile').child(id).once('value')
			    .then(function(snapshot) {
			    	console.log(snapshot.val());
			    	uid.innerHTML = snapshot.val().Uid;
				    nickname.innerHTML = snapshot.val().Nickname;
				    gender.innerHTML = snapshot.val().Gender;
				    age.innerHTML = snapshot.val().Age;
				    birthday.innerHTML = snapshot.val().Birthday;
				    friend_num .innerHTML= snapshot.val().Friends;
				    if(snapshot.val().Friends != 0) {
				    	var f_list = "";
				    	var name = snapshot.val().Nickname;
				    	var fRef = firebase.database().ref('/Friend/' + name);
				    	fRef.orderByChild("status").equalTo("OK").on('value', snapshot => {
				    		snapshot.forEach(item => {
				    			var fname = item.val().name;
				    			f_list += "<span>" + fname + "</span>";
				    		})
				    	});
				    	friend_list.innerhtml = f_list;
				    }

				    if(id == snapshot.val().Uid) {
				    	var btnAddFriend = document.getElementById('btnAddFriend');
				    	btnAddFriend.addEventListener('click', function() {
					    	var fname = document.getElementById('friend-name').value;
					    	
					    	console.log(displayName);
					    	if(user != null && fname != null) {
					    		firebase.database().ref('Profile').orderByChild("Nickname").equalTo(fname).once("value", snapshot => {
						            if(snapshot.exists()) {
						            	snapshot.forEach(item => {
						            		fid = item.key;
					    					console.log(item.key);
					    					var key = firebase.database().ref('Notification').child(fname).push().getKey();
						                a_f = {
						                	id: fid,
							    			name: fname,
							    			status: "requesting"
							    		};
							    		b_f = {
							    			id: user.uid,
							    			name: displayName,
							    			status: "requested"
							    		};
							    		b_n = {
							    			key: key,
							    			genre: 1,
							    			id: fid,
							    			name: displayName,
							    			txt: "<strong>" + user.displayName + "</strong> send you a friend request."
							    		};

							    		firebase.database().ref('Friend').child(fname).set(a_f);
							    		firebase.database().ref('Friend').child(fname).child(displayName).set(b_f);
							    		alert("Send request success");
							    		fname = "";


							    		var key = firebase.database().ref('Notification').child(fname).child(key).set(b_n);
					    				});
						            	
						            	
						            } else {
						            	alert("This name doesn't exist!")
						            	fname = "";
						            }
						        });
					    	}
					    });
				    }
			    }).catch(error => {
			    	console.log(error.message);
			    });
            });
            

        }
    });
    
    

    
    
}

window.onload = function() {
    initApp();
};