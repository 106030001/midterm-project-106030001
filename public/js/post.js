function initApp() {

    firebase.auth().onAuthStateChanged(function(user) {
        if(user) {
            var displayName;
            var username = document.getElementById("username");
            firebase.database().ref('Profile').child(user.uid).once('value', snapshot => {
                displayName = snapshot.val().Nickname;
            }).then(function() {
                username.innerHTML = displayName;
                var btnLogout = document.getElementById("logout-btn");
                btnLogout.addEventListener('click', function() {
                    firebase.auth().signOut()
                    .then(function(success) {
                        alert('Logout Success');
                        location.href = "index.html";
                    }).catch(function(error) {
                        alert('logout Error');
                    });
                });
                var sUrl = location.href;
                var post_list = document.getElementById('post-list');
                var p = "";
                var pid;
                var num, last, update;
                if(sUrl.indexOf("?") != -1) {
                    var gets = sUrl.split("?");
                    var getid = gets[1].split("=");
                    pid = getid[1];
                    console.log(pid);
                    firebase.database().ref('Post').orderByKey().equalTo(pid).once("child_added", snapshot => {
                        console.log(snapshot.val().author);
                        num = snapshot.val().comment_num;
                        var title = document.getElementById('title');
                        var author = document.getElementById('author');
                        var time = document.getElementById('time');
                        var content = document.getElementById('content');
                        var comment_list = document.getElementById('comment-list');
                        title.innerHTML = snapshot.val().title;
                        author.innerHTML = snapshot.val().author;
                        time.innerHTML = snapshot.val().time;
                        content.innerHTML = snapshot.val().content;
                        firebase.database().ref('Comment').orderByKey().equalTo(pid).once('child_added', snapshot => {
                            console.log(snapshot.val().key);
                            snapshot.forEach(item => {
                                $('#comment-list').append('<span>' + item.val().time + ' <strong>' + item.val().author + '</strong>: ' + item.val().content + '</span><br>');
                            });
                        });
                        ;
                    });
                }
                var btnComment = document.getElementById('btnComment');
                btnComment.addEventListener('click', function() {
                    var txtComment = document.getElementById('comment').value;
                    console.log(txtComment.value);
                    if(txtComment != "") {
                        console.log(txtComment);
                        var author = displayName;
                        var time = getCurrentDate();
                        var c = {
                            time: time,
                            author: author,
                            content: txtComment
                        };
                        firebase.database().ref('Post').orderByKey().equalTo(pid).on('child_added', snapshot => {
                            var c_num = snapshot.val().comment_num + 1;
                            var up = {};
                            up['comment_num'] = c_num;
                            up['last_post'] = author;
                            up['update'] = time;
                            firebase.database().ref('Post').child(pid).update(up);
                        });
                        var cRef = firebase.database().ref('Comment').child(pid).push(c);
                        txtComment = "";
                        window.location.reload();
                    } else {
                        alert("Please input something!");
                    }
                });
            });
        } else {
            alert("Please login/signup first!");
            location.href="index.html";
        }
    });
}

function getCurrentDate() {
    var currentdate = new Date();
    return currentdate.getFullYear() + "/" + currentdate.getMonth() + "/" + currentdate.getDay() + " " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
}

window.onload = function() {
    initApp();
};