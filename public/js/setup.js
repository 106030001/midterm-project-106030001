function initApp() {
    var txtNickname = document.getElementById('inputNickname');
    var dateBirthday = document.getElementById('inputBirthday');
    var genderFemale = document.getElementById('female');
    var genderMale = document.getElementById('male');
    var genderCustom = document.getElementById('custom');
    //var inputGender = document.getElementById('customgender');
    var btnSetUp = document.getElementById('btnSetUp');

    /*genderFemale.addEventListener('change', function() {
        if(!this.checked) {
            inputGender.innerHTML = "";
        }
    });
    genderMale.addEventListener('change', function() {
        if(!this.checked) {
            inputGender.innerHTML = "";
        }
    });
    genderCustom.addEventListener('change', function() {
        if(this.checked) {
            console.log("custom");
            inputGender.innerHTML = "<input type='text' id='inputCustom' class='form-control' placeholder='Gender'>";
        }
    });*/

    btnSetUp.addEventListener('click', function() {
        var user = firebase.auth().currentUser;
        var nickname = txtNickname.value;
        var birthday = dateBirthday.value;
        var profileRef = firebase.database().ref('Profile');
        var pfriendsRef = firebase.database().ref('Friends');
        var profile = {};
        var friends = {};
        var notification = {};
        console.log(birthday);
        if(user != null && nickname != null && birthday != null) {
            if(genderFemale.checked || genderMale.checked || genderCustom.checked) {
                user.displayName = nickname;
                var b = new Date(birthday.replace(/-/g, "\/")); 
                var d = new Date(); 
                var age = d.getFullYear()-b.getFullYear()-((d.getMonth()<b.getMonth()|| d.getMonth()==b.getMonth() && d.getDate()<b.getDate())?1:0);
                console.log(age);
                if(genderFemale.checked) {
                    profile = {
                        Uid: user.uid,
                        Nickname: nickname,
                        Gender: "Female",
                        Age: age,
                        Birthday: birthday,
                        Friends: 0
                    };
                } else if(genderMale.checked) {
                    profile = {
                        Uid: user.uid,
                        Nickname: nickname,
                        Gender: "Male",
                        Age: age,
                        Birthday: birthday,
                        Friends: 0
                    };
                } else {
                    //var gender = document.getElementById('inputGender').value;
                    profile = {
                        Uid: user.uid,
                        Nickname: nickname,
                        Gender: "Other",
                        Age: age,
                        Birthday: birthday,
                        Friends: 0
                    };
                }
                firebase.database().ref('Profile').child(user.uid).set(profile);
                firebase.database().ref('Friend').child(nickname).set(friends);
                firebase.database().ref('Notification').child(nickname).set(notification);
                console.log('success');
                location.href = "index.html";
            }
        }
    });
}

window.onload = function() {
    initApp();
};